const int ledPin1 = 11;  //led rojo
const int ledPin2 = 10;  //led verde
const int ledPin3 = 9;   //led azul

char incomingString[13];  //Array con 12+1 caracteres "255,255,255"+NULL donde almacenaremos el string que nos llega por serial

void setup() {
  Serial.begin(9600);
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);

  //Mientras no nos llega nada por serial, enviamos desde arduino un mensaje "hello". Esto lo hacemos porque p5js necesita recibir datos por serial para activarse
  while (Serial.available() <= 0) {
    Serial.println("hello");  // send a starting message
    delay(300);               // wait 1/3 second
  }
}

void loop() {
  int val1, val2, val3;

  if (Serial.available() > 0) {  // Si hay datos disponibles para leer...

    //Transformamos el string entrante a un array de caracteres llamado incomingString
    Serial.readStringUntil('\n').toCharArray(incomingString, 13);
    
    //Ejemplo: incomingString = {'1','2','3',',','1','6','7',',','2','5','5'}

    //Empieza a transformar los caracteres en un número hasta que detecta un caracter que no es un número, en este caso una coma. Este primer valor sería 123 según el ejemplo anterior
    val1 = atoi(incomingString);
    //Aquí le indicamos que transforme desde el índice 4 de nuestra cadena de caracteres, esto es, el quinto caracter, teniendo en cuenta el índice 0. Por lo tanto leemos después de la coma hasta que se encuentre la siguiente coma. Resultado 167
    val2 = atoi(incomingString + 4);
    //Resultado 255. Para de transformar hasta que detecta null de fin del string
    val3 = atoi(incomingString + 8);
    
    //pasamos los valores separados a cada led para cambiar su brillo :)
    analogWrite(ledPin1, val1);
    analogWrite(ledPin2, val2);
    analogWrite(ledPin3, val3);

    //Enviamos de vuelta a p5js en un string los valores de cada slider     
    Serial.print(val1);
    Serial.print(",");
    Serial.print(val2);
    Serial.print(",");
    Serial.println(val3);
  }
}