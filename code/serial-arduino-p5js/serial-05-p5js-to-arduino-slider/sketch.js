let slider;
let sliderW = 300;

const serial = new p5.WebSerial();

function setup() {
  createCanvas(400, 400);
  background(0);
  slider = createSlider(0, 255, 100);
  slider.position(width/2 - sliderW/2, height/2);
  slider.style('width', sliderW+'px');
  setPorts(); //Configurar puerto serial (set-webserial.js)
}

function draw() {
  let val = slider.value();
  background(val);
  serial.write(val);
  //console.log(val);
}

function serialEvent() {
}