//Ejemplo para enviar desde p5js a arduino

let boxX;
let boxY;
let boxSize = 20;

const serial = new p5.WebSerial();

function setup() {
  createCanvas(400, 400);
  background(0);
  boxX = width / 2.0;
  boxY = height / 2.0;
  rectMode(RADIUS);
  setPorts(); //Configurar puerto serial (set-webserial.js)
}

function draw() {
  if (mouseX > boxX - boxSize && mouseX < boxX + boxSize &&
    mouseY > boxY - boxSize && mouseY < boxY + boxSize) {
    // draw a line around the box and change its color:
    stroke(153);
    fill(153);
    // send an 'H' to indicate mouse is over square:
    serial.write('H');
  }
  else {
    // return the box to it's inactive state:
    stroke(153);
    fill(0);
    // send an 'L' to turn the LED off:
    serial.write('L');
  }
  // Draw the box
  rect(boxX, boxY, boxSize, boxSize);
}

function serialEvent() {
  console.log(serial.read());
}
