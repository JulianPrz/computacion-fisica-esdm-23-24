let posX, posY;
let circleColor = 255;

// variable para almacenar una instancia de la librería p5.webserial
const serial = new p5.WebSerial();

// variable para los datos que entren por serial
let inData;

function setup() {
  createCanvas(400, 300);
  background(0);
  setPorts(); //Configuramos los puertos seriales, función definida en set-webserial.js
}

function draw() {
  background(0);
  fill(circleColor);
  ellipse(posX, posY, 50, 50);
}

// lee cualquier dato de entrada como string
// (asume una nueva línea al final de la misma)
function serialEvent() {
  // lee un string de entrada al puerto serie
  // hasta que obtiene un retorno de carril y una línea nueva
  let inString = serial.readStringUntil("\r\n");
  
  if (inString != null) {
    let sensors = split(inString, ",");

    if (sensors.length > 2) {
      posX = map(sensors[0], 0, 255, 0, width);
      posY = map(sensors[1], 0, 255, 0, height);
      circleColor = 255 - sensors[2] * 255;
     //console.log(posX, posY, circleColor);
    }
  }
}