// 1 botón, 2 potenciómetros

int buttonPin = 3; // digital input

void setup()
{
        Serial.begin(9600);
        pinMode(buttonPin, INPUT_PULLUP);
}

void loop()
{
        int sensorValue1 = analogRead(A0)/4;
        int sensorValue2 = analogRead(A1)/4;
        int sensorValue3 = digitalRead(buttonPin);

        Serial.print(sensorValue1);
        Serial.print(",");
        Serial.print(sensorValue2);
        Serial.print(",");
        Serial.println(sensorValue3);
        //delay(10);
}