let inData; // incoming serial data

// variables circulo pots+button
let locH = 0;
let locV = 0;
let circleColor = 255;

// variables rect led
let posX;
let posY;
let boxSize = 20;

// Slider
let slider;

const serial = new p5.WebSerial();

function setup() {
  createCanvas(400, 400);
  background(0);
  posX = width / 2.0;
  posY = height / 2.0;
  rectMode(RADIUS);
  slider = createSlider(0, 255, 100);
  slider.position(20, 50);
  slider.style('width', '280px');
  setPorts(); //Configurar puerto serial (set-webserial.js)
}


function draw() {
  //READ: dibujamos el círculo con los potenciometros de arduino, se muestra solo si pulsas el boton
  background(0);
  fill(circleColor); // fill depends on the button
  stroke(circleColor);
  ellipse(locH, locV, 50, 50); 

  //WRITE: encendemos el led si ponemos el ratón encima del rect
  fill(255);
  if (mouseX > posX - boxSize && mouseX < posX + boxSize &&
    mouseY > posY - boxSize && mouseY < posY + boxSize) {
    stroke(255);
    fill(153);
    // envía 'H' para encender LED
    serial.write('H');
  }
  else {
    stroke(153);
    fill(0);
    // envía 'L' para apagar LED
    serial.write('L');
  }
  rect(posX, posY, boxSize, boxSize);
}

// read any incoming data as a string
// (assumes a newline at the end of it):
function serialEvent() {
  // read a string from the serial port
  // until you get carriage return and newline:
  var inString = serial.readStringUntil("\r\n");

  //check to see that there's actually a string there:
  if (inString) {
    if (inString !== "hello") {
      //console.log(inString);
      // if you get hello, ignore it
      // split the string on the commas:
      var sensors = split(inString, ",");
      if (sensors.length > 2) {
        // if there are three elements
        // element 0 is the locH:
        locH = map(sensors[0], 0, 1023, 0, width);
        // element 1 is the locV:
        locV = map(sensors[1], 0, 1023, 0, height);
        // element 2 is the button:
        circleColor = 255 - sensors[2] * 255;
        // send a byte back to prompt for more data:
        //serial.write("x");
      }
    }
  }
}