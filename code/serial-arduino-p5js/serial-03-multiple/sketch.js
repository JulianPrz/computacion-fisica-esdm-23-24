// variables donde almacenaremos los valores de cada componente de arduino
let val1 = 0;
let val2 = 0;
let val3 = 0;

// variable para almacenar una instancia de la librería p5.webserial
const serial = new p5.WebSerial();

function setup() {
  createCanvas(400, 400);
  setPorts(); //Configuramos los puertos seriales, función definida en set-webserial.js
}

function draw() {
    //Aquí lo que queramos hacer con las diferentes variables val1, val2, val3
}

//evento que se dispara cuando entren datos por puerto serie
function serialEvent() {
  // Lee un String desde el puerto serie:
  let inString = serial.readStringUntil("\r\n");
    //console.log(inString);
    
  // Comprueba si realmente hay un string en el puerto
  if (inString != null) {
      let sensors = split(inString, ",");
      //Para cerciorarnos que siempre llega la lectura de 3 componentes
    if (sensors.length > 2) {
      val1 = map(sensors[0], 0, 1023, ..., ...); //Completa aquí con tu rango que quieras mapear
      val2 = map(sensors[1], 0, 1023, ..., ...); //Completa aquí con tu rango que quieras mapear
      val3 = map(sensors[2], 0, 1, ..., ...); //Completa aquí con tu rango que quieras mapear
      console.log(sensors);
    }
  }
}
