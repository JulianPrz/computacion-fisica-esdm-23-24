//Ejemplo en el que enviamos datos de diferentes sensores por comunicación serial a p5js.
//En este caso tenemos que usar si o si Serial.println() para enviar lo datos a p5js.
//Concatenamos los valores con comas entre medias para así luego poder separarlos en p5js.

int pinButton = 3;
int buttonValue = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(pinButton, INPUT_PULLUP);
}

void loop()
{
  int pot1 = analogRead(A0); // valores entre 0-1023
  int pot2 = analogRead(A1); //0-1023
  buttonValue = digitalRead(pinButton); //0-1

  Serial.print(pot1);
  Serial.print(",");
  Serial.print(pot2);
  Serial.print(",");
  Serial.println(buttonValue);
  delay(10); // pequeño delay para estabilizar señal
}
