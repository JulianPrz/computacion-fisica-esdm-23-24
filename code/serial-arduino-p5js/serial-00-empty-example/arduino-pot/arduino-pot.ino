//Ejemplo en el que enviamos datos por comunicación serial a p5js.

//Si envías datos entre 0-255, usa Serial.write()
//Si envías datos entre 0-1023, usa Serial.println()
void setup() {
  Serial.begin(9600);
}

void loop() {
  int pot = analogRead(A0); //valores entre 0-1023
	// int pot = analogRead(A0)/4; //valores entre 0-255
  Serial.println(pot); //envio por serial valores 0-1023
	// Serial.write(pot); //envio por serial valores entre 0-255
  delay(10); //pequeño delay para estabilizar señal                                            
}
