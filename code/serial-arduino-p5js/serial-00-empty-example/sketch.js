//Este sketch recibe la señal de arduino.

//Si desde arduino enviamos la señal con Serial.write(), es decir,
//cuando enviamos valores entre 0 y 255, utilizamos la parte comentada
//en serialEvent().

//Si desde arduino enviamos la señal con Serial.println(), es decir,
//cuando enviamos valores entre 0 y 1023, utilizamos la parte no
//comentada en serialEvent()

const serial = new p5.WebSerial(); //objeto webserial

let inData;
let xPos = 0;
let yPos = 0;

function setup() {
    createCanvas(400, 400);
    setPorts(); //Con setPorts() configuramos la comunicación serial desde el navegador
}

function draw() {
//Aquí lo que quieras hacer con inData o inString
}

function serialEvent() {

    //Para cuando enviamos desde arduino con Serial.println()
    let inString = serial.readLine();
    console.log(inString);

    //Para cuando enviamos desde arduino con Serial.write()
    // if(inString != null){
    //     inData = Number(inString);
    // }
    //console.log(inData);
}

