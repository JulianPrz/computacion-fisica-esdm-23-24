let xPos = 0;

// variable para almacenar una instancia de la librería p5.webserial
const serial = new p5.WebSerial();

// variable para los datos que entren por serial
let inData;

function setup() {
  createCanvas(400, 300);
  background("darkblue");
  setPorts(); //Configuramos los puertos seriales, función definida en set-webserial.js
}

function draw() {
  graphData(inData); //dibujamos la gráfica con los datos que entren por serial
}

function graphData(newData) {
  // mapeamos el rango de los datos de entrada con la altura del canvas
  let yPos = map(newData, 0, 255, 0, height);

  stroke("lightblue");
  line(xPos, height, xPos, height - yPos);

  //cuando la línea llegue al final del canvas, reseteamos su posición a 0
  if (xPos >= width) {
    xPos = 0;
    background("darkblue");
  } else {
    xPos++;
  }
}

//evento que se dispara cuando entren datos por puerto serie
function serialEvent() {
  // Lee un String desde el puerto serie:
  let inString = serial.readLine();
  //console.log(inString);
  // Comprueba si realmente hay un string en el puerto
  if (inString != null) {
    // Lo convierte a número
    inData = Number(inString);
  }
  //console.log(inData); //Imprimimos en consola los datos, esto puede saturar el proceso
}