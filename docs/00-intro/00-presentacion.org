#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:Presentación asignatura
#+SUBTITLE: Computación Física
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:julian.perezromero@educa.madrid.org
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_EXTRA_CSS: ../../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="title" style="text-transform:uppercase;font-size:2em">%t</h1><h3 class="subtitle">%s</h3><br><br><h4>%a</h4><br><p>Máster de Diseño Interactivo</p><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Índice
# Generar TOC
# org-reveal-manual-toc
  - [[Hola!👋][Hola!]]
  - [[Temario][Temario]]
  - [[Evaluación][Evaluación]]
  - [[Cronograma][Cronograma]]
  - [[Seguimiento][Seguimiento]]
  - [[Recomendaciones][Recomendaciones]]
  - [[Materiales][Materiales]]
* Hola!👋
Artista visual y docente en nuevos medios. Participo activamente en
proyectos que promuevan la Cultura Libre de manera colaborativa y
trabajo con proyectos y comunidades que giran en torno la programación
creativa y la fabricación digital
* Cuestionario :noexport:
- Link a la encuesta:
  http://encuestas.educa.madrid.org/index.php/531232?lang=es
* Temario
** I - Principios de Computación Física
 1. Conexión a sensores 
 2. Actuadores
 3. Conexión Arduino y Processing
** II - Elementos de Programación Creativa
 1. Presentación asignatura
 2. Introducción a la Programación Creativa
 3. Funciones y variables
 4. Primitivas formales 
 5. Bucles y condiciones
 6. Imágenes y Píxeles 
 7. Texto y tipografía 
** III - Casos de Estudio
* Evaluación
** Evaluación continua
- Participación activa / actitud *10%*
- Tareas / ejercicios prácticos *90%*
- *IMPORTANTE:* Para superar la evaluación continua es obligatorio presentar y superar con al menos 5/10 todas las prácticas
** Evaluación perdida ev. continua
- Pruebas escritas *30%*
- Pruebas prácticas *70%*
- Se pierde evaluación continua si hay falta de asistencia del 20% de las clases = *4 ausencias(>6,4h) injustificadas*
** Evaluación Extraordinaria
- Pruebas escritas *30%*
- Pruebas prácticas *70%*
* Cronograma
- 29/02: Intro asignatura. Intro Arduino
- 07/03: Arduino sensores
- 14/03: Arduino actuadores
- 21/03: Intro Processing 
- 04/04: Processing: Movimiento, Bucles y condicionales.
- 11/04: Comunicación Arduino y Processing
* Metodología
- Clases teórico-prácticas
- Prácticas semanales
- Trabajo final: Caso de estudio
* Seguimiento
- [[https://aulavirtual38.educa.madrid.org/esd.madrid/][Aula virtual]]
- [[https://pad.riseup.net/p/newmedia-keep][Pad Colaborativo]]
- [[https://gitlab.com/JulianPrz/computacion-fisica-23-24][Repositorio código]]
* Recomendaciones
- Tener independencia para resolver errores, saber encontrar fuentes de información
- Pensar en la lógica de la programación
- Llevar un progreso constante, repasar en casa
- Plantear ideas, dudas, reflexiones
- Si vas con buen ritmo ayuda a tus compañerxs
* Materiales
- Es recomendable haceros con un kit básico
- La escuela dispone de placas Arduino y sensores para las prácticas en clase
- [[https://docs.google.com/spreadsheets/d/10_qAWsZZ0XATJ1m8wzproM92K2iFoNCKcvUsKL-cqeI/edit#gid=0][Link a inventario electrónica]] 
- No hay definido protocolo de préstamo
* Siguiente \to                                                    :noexport:
   :PROPERTIES:
   :reveal_background: #FFCC00
   :END:
#+REVEAL_HTML: <a href="https://julianprz.gitlab.io/computacion-fisica-21-22/master/docs/00-Intro/01-intro-asignatura.html" class="r-fit-text" target="_blank">01-Introducción a la Programación Creativa</h2>
